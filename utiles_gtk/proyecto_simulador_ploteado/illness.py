# Illnes

class Illness():
    """ Clase dedicada al objeto enfermedad de base """
    def __init__(self, name, gravity):
        # Se establece la enfermedad
        self.name = name
        self.gravity = gravity


    @classmethod
    # Asma
    def asthma(cls):
        return cls("Asma", 0.7)

    @classmethod
    # Enfermedad cerebrovascular
    def cereb_disease(cls):
        return cls("Enfermedad cerebrovascular", 0.6)

    @classmethod
    # Fibrosis quistica
    def cystic_fibrosis(cls):
        return cls("Fibrosis quistica", 0.9)

    @classmethod
    # Hipertension
    def hypertension(cls):
        return cls("Hipertension", 0.4)

    @classmethod
    # Sin enfermedad
    def no_illness(cls):
        return cls("Sin enfermedad", 0)
