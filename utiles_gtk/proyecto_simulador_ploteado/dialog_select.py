import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gio
from gi.repository import GdkPixbuf

class DialogSelect(Gtk.Dialog):
    def __init__(self, parent, data):
        super().__init__(title="Seleccionar ejes", transient_for = parent,
        flags=0)
        self.add_buttons(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OK, Gtk.ResponseType.OK
        )

        self.set_default_size(150, 100)
        self.box = self.get_content_area()
        self.grid = Gtk.Grid()
        self.grid.set_row_spacing(20)
        self.grid.set_column_spacing(20)
        self.box.add(self.grid)

        # Tener dos comboboxtexts con los ejes a seleccionar
        self.x_axis = Gtk.ComboBoxText()
        self.y_axis = Gtk.ComboBoxText()
        for axis in data.keys():
            self.x_axis.append_text(axis)
            self.y_axis.append_text(axis)
        self.x_axis.set_active(0)
        self.y_axis.set_active(0)

        self.lbl_x = Gtk.Label(label = "Eje x:")
        self.lbl_y = Gtk.Label(label = "Eje y:")

        self.grid.attach(self.lbl_x, 0, 0, 1, 1)
        self.grid.attach_next_to(self.x_axis, self.lbl_x,
        Gtk.PositionType.RIGHT, 1, 1)
        self.grid.attach_next_to(self.lbl_y, self.lbl_x,
        Gtk.PositionType.BOTTOM, 1, 1)
        self.grid.attach_next_to(self.y_axis, self.lbl_y,
        Gtk.PositionType.RIGHT, 1, 1)

        self.show_all()
