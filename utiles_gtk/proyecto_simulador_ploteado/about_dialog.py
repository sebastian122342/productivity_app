# About dialog

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

import urllib.request

url = "-------"

class AboutDialog(Gtk.AboutDialog):
    def __init__(self):
        super().__init__(title="Dialogo About")
        self.set_modal(True)
        autores = ["Sebastián Bustamante"]
        self.add_credit_section("Autor", autores)
        self.set_comments("Primera Guia Unidad 2, Programacion Avanzada")
        self.set_logo_icon_name("input-gaming")
        self.set_program_name("Validacion de Aprobacion")
        self.set_version("1.0")
        self.set_website(url)
        self.set_website_label("Repositorio con la guia")
        self.connect("activate-link", self.abrir_link)

        self.show_all()

    def abrir_link(self, cosita, nose):
        urllib.request.urlopen(self.get_website())
