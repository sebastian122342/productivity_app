# Main

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gio
from gi.repository import GdkPixbuf

from about_dialog import AboutDialog

from messages import on_error_clicked
from messages import on_info_clicked
from messages import on_question_clicked


class MainWindow(Gtk.Window):
    def __init__(self):
        super().__init__()
        self.set_default_size(600, 600)
        self.set_border_width(20)
        self.connect("destroy", Gtk.main_quit)

        # Se crea la grid que contendrá los demas widgets
        self.grid = Gtk.Grid()
        self.grid.set_row_spacing(20)
        self.grid.set_column_spacing(20)
        self.add(self.grid)

        # Se crea la headerbar
        self.hb = Gtk.HeaderBar()
        self.hb.set_title("Verificacion de Aprobacion")
        self.hb.set_subtitle("Guia 1 Unidad 2 - Programacion Avanzada")
        self.hb.set_show_close_button(True)
        self.set_titlebar(self.hb)

        # Boton Ventana About
        self.btn_about = Gtk.Button()
        icon = Gio.ThemedIcon(name="help-about")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.btn_about.add(image)
        self.btn_about.connect("clicked", self.on_btn_about)
        self.hb.pack_end(self.btn_about)

        # Label para el nombre del alumno
        self.lbl_nombre = Gtk.Label(label="Nombre Alumno: ")
        self.grid.attach(self.lbl_nombre, 0, 0, 1, 1)

        # Label para el nombre del ramo
        self.lbl_ramo = Gtk.Label(label="Ramo: ")
        self.grid.attach(self.lbl_ramo, 0, 1, 1, 1)

        # Label para la nota
        self.lbl_nota = Gtk.Label(label="Nota: ")
        self.grid.attach(self.lbl_nota, 0, 2, 1, 1)

        # Entry para el nombre del alumno
        self.entry_nombre = Gtk.Entry()
        self.entry_nombre.set_placeholder_text("Nombre del alumno")
        self.grid.attach(self.entry_nombre, 1, 0, 1, 1)

        # Entry para el ramo
        self.entry_ramo = Gtk.Entry()
        self.entry_ramo.set_placeholder_text("Nombre del ramo")
        self.grid.attach(self.entry_ramo, 1, 1, 1, 1)

        # Comboboxtext para las notas
        self.opciones_nota = Gtk.ComboBoxText()
        for x in range(1, 11):
            self.opciones_nota.append_text(str(x))
        self.opciones_nota.set_active(0)
        self.grid.attach(self.opciones_nota, 1, 2, 1, 1)

        # Boton para resetear todo
        self.btn_reset = Gtk.Button(label="Reset")
        self.btn_reset.connect("clicked", self.on_btn_reset)
        self.grid.attach(self.btn_reset, 0, 3, 1, 1)

        # Boton de procesar info dada
        self.btn_procesar = Gtk.Button(label="Procesar informacion")
        self.btn_procesar.connect("clicked", self.on_btn_procesar)
        self.grid.attach(self.btn_procesar, 1, 3, 1, 1)


    def on_btn_about(self, btn):
        AboutDialog()


    def on_btn_reset(self, btn):
        principal = "Reseteo de informacion"
        secundaria = "¿Seguro que quiere Resetear?"
        if not on_question_clicked(self,principal, secundaria):
            return
        self.opciones_nota.set_active(0)
        self.entry_nombre.set_text("")
        self.entry_ramo.set_text("")


    def on_btn_procesar(self, btn):
        nombre = self.entry_nombre.get_text().strip(" ")
        ramo = self.entry_ramo.get_text().strip(" ")

        # Para procesar tiene que haber un nombre y ramo ingresado
        if nombre == "" or ramo == "":
            principal = "Error al procesar la informacion"
            secundaria = "Se debe ingresar un nombre y un ramo"
            on_error_clicked(self, principal, secundaria)
            if nombre == "":
                self.entry_nombre.set_text("")
            if ramo == "":
                self.entry_ramo.set_text("")
            return

        principal = "Resultado del procesamiento"
        nota = int(self.opciones_nota.get_active_text())
        if nota < 5:
            estado = "reprobo"
        else:
            estado = "aprobo"
        secundaria = f"{nombre} {estado} {ramo} con nota {nota}"
        on_info_clicked(self, principal, secundaria)
        self.opciones_nota.set_active(0)
        self.entry_nombre.set_text("")
        self.entry_ramo.set_text("")


if __name__ == "__main__":
    window = MainWindow()
    window.show_all()
    Gtk.main()
