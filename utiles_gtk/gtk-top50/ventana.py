import pandas
import gi
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk, Gio
from gi.repository import GdkPixbuf


"""
Este ejemplo fue preparado para dar a conocer de un modo mejor elaborado
y sin mucha participación de Glade el uso de algunos widget esenciales para
nuestra asignatura.

En general se aplica:

* Uso de headerbar, una barra avanzada y personalizable para el manejo de menú
contextual.

* Manejo de evento de row-changed en treeview o liststore.

* Ocultamiento de columnas visibles en liststore.

* Creación de botones desde código e insertados en HeaderBar.

* Uso de propiedades y eventos propios de widgets.

* set de Iconos

* Alineamiento de la posición en los widgets.

* Uso de Gtk.Paned para división de pantalla.

* Redimensionado automático de paned (avanzado)

* Uso mejorado de Gtk.Label, markup

* Ventanas de dialogos auxiliares construidas desde código.

"""

class Spotify():
    def __init__(self):

        builder = Gtk.Builder()
        builder.add_from_file("./ui/spotify.ui")

        self.window = builder.get_object("main")
        self.window.connect("destroy", Gtk.main_quit)
        self.window.maximize()

        # Crea widget de Headerbar
        hb = Gtk.HeaderBar()
        hb.set_show_close_button(True)
        hb.props.title = "Ejemplo varios widgets"
        # Se agrega headerbar a la ventana como un titulo
        self.window.set_titlebar(hb)

        # Crea un contenedor (hbox) para agregar varios botones.
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        Gtk.StyleContext.add_class(box.get_style_context(), "linked")

        # Se crea un boton
        button = Gtk.Button()
        # Se añade icono
        # https://lazka.github.io/pgi-docs/#Gtk-3.0/constants.html#Gtk.STOCK_OPEN
        icon = Gio.ThemedIcon(name="document-open")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        button.add(image)
        # evento para el boton
        button.connect("clicked", self.open_filechooser)
        # se agrega al box
        box.add(button)
        # se agrega el box (contenedor) al inicio de izquierda a derecha
        hb.pack_start(box)

        # La diferencia con el boton anterior es que este se agregará directo
        # al headerbar sin el uso de un Gtk.box. Se muestran los dos métodos.
        button = Gtk.Button()
        icon = Gio.ThemedIcon(name="help-about")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        button.add(image)
        # Se agrega al final (margen a la derecha)
        hb.pack_end(button)
        # evento para este boton
        button.connect("clicked", self.open_about_dialog)

        # treeview as liststore
        self.tree = builder.get_object("tree")
        # se asigna el evento al tree
        self.tree.connect("row-activated", self.tree_row_activated)
        # row activate por defecto se activa al doble click, pero se
        # fuerza para que sea solo un evento a un click
        # https://lazka.github.io/pgi-docs/index.html#Gtk-3.0/classes/TreeView.html#Gtk.TreeView.set_activate_on_single_click
        self.tree.set_activate_on_single_click(True)

        # get label for information
        self.information = builder.get_object("label_info")
        # se aplican margenes al widget
        # https://lazka.github.io/pgi-docs/index.html#Gtk-3.0/classes/Widget.html#Gtk.Widget.set_halign
        self.information.set_halign(Gtk.Align.START)
        self.information.set_valign(Gtk.Align.START)
        # https://lazka.github.io/pgi-docs/index.html#Gtk-3.0/classes/Widget.html#Gtk.Widget.set_margin_start
        self.information.set_margin_start(20)
        # https://lazka.github.io/pgi-docs/index.html#Gtk-3.0/classes/Widget.html#Gtk.Widget.set_margin_top
        self.information.set_margin_top(20)

        # Gtk.Paned
        paned = builder.get_object("paned_window")
        # Se crea evento para Gtk.Paned
        paned.connect("size-allocate", self.paned_resize_horizontal)

        self.window.show_all()

    def paned_resize_horizontal(self, widget, allocation):
        # Valor por defecto, al 100% de la posición actual
        # de la ventana
        hpane_pos = 1
        if allocation.width != 1:
            # siempre se redimensionda al 60% de la ventana.
            hpane_pos = 0.6 * allocation.width
        # Aplica resultado expresado en la dimesion real de la ventana
        widget.set_position(int(hpane_pos + .5))

    def crea_vista(self, pathfile):

        # lee el archivo csv
        data = pandas.read_csv(pathfile)

        if self.tree.get_columns():
            for column in self.tree.get_columns():
                self.tree.remove_column(column)

        largo_columnas = len(data.columns)
        modelo = Gtk.ListStore(*(largo_columnas * [str]))
        self.tree.set_model(model=modelo)

        cell = Gtk.CellRendererText()

        for item in range(len(data.columns)):
            column = Gtk.TreeViewColumn(data.columns[item],
                                        cell,
                                        text=item)
            self.tree.append_column(column)
            column.set_sort_column_id(item)
            # Si existen más de 4 columnas se ocultan
            if item > 3:
                # Se ocultan las columnas
                column.set_visible(False)

        for item in data.values:
            line = [str(x) for x in item]
            modelo.append(line)

    def tree_row_activated(self, model, path, iter_):
        """ Evento de cambio para el treeview, un cambio refiere
        a un cambio en la posición o selección de una fila.
        Esto es un cambio en el cursor.
        """
        model, it = self.tree.get_selection().get_selected()
        if model is None:
            return False
        # Cada cambio de cursor se ve reflejado en el Gtk.Label
        text = "<b>Datos adicionales: </b> \n\n"
        for i in range(5, len(self.tree.get_columns())):
            text = f"{text}<b>{self.tree.get_column(i).get_title()}: </b>"
            text = f"{text} {model.get_value(it, i)}"
            text = f"{text}\n"
        # Aplica texto concatenado al label, con los valores de las
        # columnas ocultas del tree
        self.information.set_markup(text)

    def open_about_dialog(self, btn=None):
        """Dialogo de acerca de.
        Se aplican varias funciones disponibles de la API
        https://lazka.github.io/pgi-docs/index.html#Gtk-3.0/classes/AboutDialog.html#Gtk.AboutDialog
        """
        about = Gtk.AboutDialog()
        about.set_modal(True)
        about.set_title("App Example")
        about.set_program_name("Programación 1")
        about.set_name("Example")
        about.set_authors(["Fabio Durán-Verdugo"])
        about.set_comments("This example is for Programación 1 at Bioinformática Utalca")
        about.set_logo_icon_name("go-home")

        about.run()
        about.destroy()

    def open_filechooser(self, btn=None):
        """Dialogo de selección de archivo
        Se aplican varias funciones disponibles en la API.
        https://lazka.github.io/pgi-docs/index.html#Gtk-3.0/classes/FileChooser.html#Gtk.FileChooser
        """

        filechooser = Gtk.FileChooserDialog()
        filechooser.set_title("Seleccione un archivo")
        # filechooser.set_parent(self.window)
        filechooser.add_buttons(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                Gtk.STOCK_OPEN, Gtk.ResponseType.OK)
        filechooser.set_action(Gtk.FileChooserAction.OPEN)

        filter_csv = Gtk.FileFilter()
        filter_csv.add_mime_type("text/csv")
        filter_csv.set_name("Archivos CSV")
        filechooser.add_filter(filter_csv)

        response = filechooser.run()

        if response == Gtk.ResponseType.OK:
            filepath = filechooser.get_filename()
            self.crea_vista(filepath)

        filechooser.destroy()


Spotify()
Gtk.main()
