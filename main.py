# codigo main de la app

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gio
from gi.repository import GdkPixbuf

from about_dialog import AboutDialog

from messages import on_error_clicked, on_info_clicked, on_question_clicked

import psycopg2

class MainWindow(Gtk.Window):
    def __init__(self):
        super().__init__()
        self.maximize()
        self.set_border_width(20)
        self.connect("destroy", Gtk.main_quit)
        
        # Se crea la grid que contendrá los demas widgets
        self.grid = Gtk.Grid()
        self.grid.set_row_spacing(20)
        self.grid.set_column_spacing(20)
        self.add(self.grid)

        # Se crea la headerbar
        self.hb = Gtk.HeaderBar()
        self.hb.set_title("Productivity app")
        self.hb.set_subtitle("por amor al arte")
        self.hb.set_show_close_button(True)
        self.set_titlebar(self.hb)

        # Boton Ventana About
        self.btn_about = Gtk.Button()
        icon = Gio.ThemedIcon(name="help-about")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.btn_about.add(image)
        self.btn_about.connect("clicked", self.on_btn_about)
        self.hb.pack_end(self.btn_about)

    def on_btn_about(self, btn):
        AboutDialog()

if __name__ == "__main__":
    window = MainWindow()
    window.show_all()
    Gtk.main()
